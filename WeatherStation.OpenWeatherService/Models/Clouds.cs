﻿using Newtonsoft.Json;

namespace WeatherStation.OpenWeatherService.Models
{
    public class Clouds
    {
        [JsonProperty("all")]
        public int Description { get; set; }
    }
}
