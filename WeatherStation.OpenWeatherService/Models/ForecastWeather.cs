﻿using Newtonsoft.Json;

namespace WeatherStation.OpenWeatherService.Models
{
    public class ForecastWeather : Weather
    {
        [JsonProperty("pop")]
        public float ChanceOfRain { get; set; }

        [JsonProperty("sys")]
        public PartOfDay PartOfDay { get; set; }

        [JsonProperty("dt_txt")]
        public string DateString { get; set; }
    }
}
