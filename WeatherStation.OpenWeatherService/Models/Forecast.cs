﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WeatherStation.OpenWeatherService.Models
{
    public class Forecast
    {
        [JsonProperty("cod")]
        public string WeatherCode { get; set; }

        [JsonProperty("message")]
        public int Message { get; set; }

        [JsonProperty("cnt")]
        public int Count { get; set; }

        [JsonProperty("list")]
        public List<ForecastWeather> Weather { get; set; }

        [JsonProperty("city")]
        public City City { get; set; }
    }
}
