﻿using Newtonsoft.Json;

namespace WeatherStation.OpenWeatherService.Models
{
    public class Precipitation
    {
        [JsonProperty("1h", NullValueHandling = NullValueHandling.Ignore)]
        public float OneHour { get; set; }

        [JsonProperty("3h", NullValueHandling = NullValueHandling.Ignore)]
        public float ThreeHour { get; set; }
    }
}
