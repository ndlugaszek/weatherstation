﻿using Newtonsoft.Json;

namespace WeatherStation.OpenWeatherService.Models
{
    public class Coordinates
    {
        [JsonProperty("lon")]
        public float Longtitude { get; set; }

        [JsonProperty("lat")]
        public float Latitude { get; set; }
    }
}
