﻿using Newtonsoft.Json;

namespace WeatherStation.OpenWeatherService.Models
{
    public class TemperatureAndPressure
    {
        [JsonProperty("temp")]
        public float Temperature { get; set; }

        [JsonProperty("feels_like")]
        public float FeelsLike { get; set; }

        [JsonProperty("temp_min")]
        public float TemperatureMin { get; set; }

        [JsonProperty("temp_max")]
        public float TemperatureMax { get; set; }

        [JsonProperty("pressure")]
        public int Pressure { get; set; }

        [JsonProperty("humidity")]
        public int Humidity { get; set; }

        [JsonProperty("sea_level")]
        public int SeaLevelPressure { get; set; }

        [JsonProperty("grnd_level")]
        public int GroundLevelPressure { get; set; }

        [JsonProperty("temp_kf")]
        public float TemperatureKf { get; set; }
    }
}
