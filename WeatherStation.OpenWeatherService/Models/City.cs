﻿using Newtonsoft.Json;

namespace WeatherStation.OpenWeatherService.Models
{
    public class City : Site
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("coord")]
        public Coordinates Coordinates { get; set; }

        [JsonProperty("population")]
        public int Population { get; set; }

        [JsonProperty("timezone")]
        public int Timezone { get; set; }
    }
}
