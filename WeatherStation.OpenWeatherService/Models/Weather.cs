﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WeatherStation.OpenWeatherService.Models
{
    public class Weather
    {
        [JsonProperty("dt")]
        public int Date { get; set; }

        [JsonProperty("main")]
        public TemperatureAndPressure TemperatureAndPressure { get; set; }

        [JsonProperty("weather")]
        public List<WeatherDescription> WeatherDescriptions { get; set; }

        [JsonProperty("clouds")]
        public Clouds Clouds { get; set; }

        [JsonProperty("wind")]
        public Wind Wind { get; set; }

        [JsonProperty("visibility")]
        public int Visibility { get; set; }

        [JsonProperty("rain")]
        public Precipitation Rain { get; set; }

        [JsonProperty("snow")]
        public Precipitation Snow { get; set; }


    }
}
