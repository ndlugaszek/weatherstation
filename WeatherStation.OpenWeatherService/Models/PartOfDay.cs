﻿using Newtonsoft.Json;

namespace WeatherStation.OpenWeatherService.Models
{
    public class PartOfDay
    {
        [JsonProperty("pod")]
        public string DayOrNight { get; set; }
    }
}
