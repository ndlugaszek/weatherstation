﻿using Newtonsoft.Json;

namespace WeatherStation.OpenWeatherService.Models
{
    public class CurrentWeather : Weather
    {
        [JsonProperty("coord")]
        public Coordinates Coordinates { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("sys")]
        public Site SiteInfo { get; set; }

        [JsonProperty("timezone")]
        public int Timezone { get; set; }

        [JsonProperty("id")]
        public int SiteId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cod")]
        public int WeatherCode { get; set; }
    }
}
