﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using WeatherStation.Common.Interfaces;
using WeatherStation.Common.Models;
using WeatherStation.OpenWeatherService.Clients;
using WeatherStation.OpenWeatherService.Interfaces;
using WeatherStation.OpenWeatherService.Models;

namespace WeatherStation.OpenWeatherService.Services
{
    /// <summary>
    /// <inheritdoc cref="IWeather"/>
    /// Uses external OpenWeather API to get the information about the weather.
    /// </summary>
    public class OpenWeatherService : IWeather
    {
        /// <summary>
        /// Declaration of the IOpenWeatherClient field
        /// </summary>
        private readonly IOpenWeatherClient CLIENT;

        /// <summary>
        /// Number of the forecasts for a one day
        /// </summary>
        private const int NUMBER_OF_FORECASTS_IN_A_DAY = 8;

        public OpenWeatherService(IOpenWeatherClient client)
        {
            this.CLIENT = client;
        }

        /// <inheritdoc cref="IWeather.GetCurrentWeather"/>
        public Common.Models.Weather GetCurrentWeather(Location location)
        {
            var json = CLIENT.CallCurrentWeather(location).Result;
            var currentWeather = JsonConvert.DeserializeObject<CurrentWeather>(json);

            return new Common.Models.Weather
            {
                Conditions = currentWeather.WeatherDescriptions.FirstOrDefault()?.Description,
                Temperature = currentWeather.TemperatureAndPressure.Temperature,
                ChanceOfRain = currentWeather.Rain?.ThreeHour ?? 0,
                Location = new ExtendedLocation(currentWeather.Name, location)
            };
        }

        /// <inheritdoc cref="IWeather.GetWeatherForecast"/>
        public IDictionary<DateTime, Common.Models.Weather> GetWeatherForecast(Location location)
        {
            var json = new OpenWeatherClient().CallWeatherForecast(location).Result;
            var forecastWeather = JsonConvert.DeserializeObject<Forecast>(json);

            return forecastWeather.Weather
                .Where((forecast, index) => index % NUMBER_OF_FORECASTS_IN_A_DAY == 0)
                .Select(WeatherForecastForEachDay)
                .ToDictionary(k => k.Key, v => v.Value);
        }

        /// <summary>
        /// Creates KeyValuePair of the DateTime and Common.Models.Weather with needed information
        /// </summary>
        /// <param name="weather">Weather object with converted JSON data</param>
        /// <param name="index">int argument with the index value (start from 0)</param>
        /// <returns>KewyValuePair, which contains DateTime with date and Common.Models.Weather objects with description, temperature and chance of rain</returns>
        private KeyValuePair<DateTime, Common.Models.Weather> WeatherForecastForEachDay(Models.Weather weather, int index)
        {
            var forecast = new Common.Models.Weather
            {
                ChanceOfRain = weather.Rain?.ThreeHour ?? 0,
                Conditions = weather.WeatherDescriptions.FirstOrDefault()?.Description,
                Temperature = weather.TemperatureAndPressure.Temperature
            };
            var date = DateTime.Now.AddDays(index + 1);

            return new KeyValuePair<DateTime, Common.Models.Weather>(date, forecast);
        }
    }
}
