﻿using System.Threading.Tasks;
using WeatherStation.Common.Models;

namespace WeatherStation.OpenWeatherService.Interfaces
{
    /// <summary>
    /// Represents the client of the OpenWeather service
    /// </summary>
    public interface IOpenWeatherClient
    {
        /// <summary>
        /// Takes the given location and sends the request to the OpenWeather service to gets the current weather
        /// </summary>
        /// <param name="location">Location with the latitude and the longitude</param>
        /// <returns>String task object with JSON response</returns>
        Task<string> CallCurrentWeather(Location location);

        /// <summary>
        /// Takes the given location and sends the request to the OpenWeather service to gets the weather forecast
        /// </summary>
        /// <param name="location">Location with the latitude and the longitude</param>
        /// <returns>String task object with JSON response</returns>
        Task<string> CallWeatherForecast(Location location);
    }
}
