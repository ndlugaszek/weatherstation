﻿using System;
using System.Net;
using System.Threading.Tasks;
using WeatherStation.Common.Models;
using WeatherStation.OpenWeatherService.Interfaces;

namespace WeatherStation.OpenWeatherService.Clients
{
    /// <inheritdoc cref="IOpenWeatherClient"/>
    public class OpenWeatherClient : IOpenWeatherClient, IDisposable
    {
        /// <summary>
        /// The constant variable, which contains the APIKEY of the OpenWeatherAPI
        /// </summary>
        private const string APIKEY = "30ec0a8a7db3999881d765b0f37c002a";

        /// <summary>
        /// Represents the instance of the WebClient
        /// </summary>
        private readonly WebClient webClient = new();

        public async Task<string> CallCurrentWeather(Location location)
        {
            var url = BuildUrl(location.Latitude, location.Longitude, "weather");
            return await webClient.DownloadStringTaskAsync(url);
        }

        public async Task<string> CallWeatherForecast(Location location)
        {
            var url = BuildUrl(location.Latitude, location.Longitude, "forecast");
            return await webClient.DownloadStringTaskAsync(url);
        }

        /// <summary>
        /// Takes the latitude, longitude and weatherType to creates the URL
        /// </summary>
        /// <param name="latitude">Float with latitude</param>
        /// <param name="longitude">Float with longitude</param>
        /// <param name="weatherType">String with <c>weather</c> or <c>forecast</c> argument</param>
        /// <returns>String object with URL</returns>
        private static string BuildUrl(float latitude, float longitude, string weatherType)
        {
            var url = $"https://api.openweathermap.org/data/2.5/{weatherType}?lat={latitude}&lon={longitude}&appid={APIKEY}&units=metric&lang=pl";
            return url;
        }

        public void Dispose()
        {
            webClient.Dispose();
        }
    }
}