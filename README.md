# weatherstation
Training app that functions as a weather station

## Goal
The goal of the project is to create library consumed by console app to display weather data using free and open API endpoints. The library should be tested using unit tests.

### Features
- Configurable APIs
- Geocoding
- Displaying current weather
- Displaying weather forecast
- Displaying air quality

### Flow
1. User is asked for where they live. They input the info and accept it by hitting `Enter`.
2. If user wrote coordinates, proceed to 3. Otherwise, geocode their place and give them choice to pick best option.
3. Show user current weather from all configured weather APIs.
4. Show user weather forecast for next 3 days using configured APIs.
5. Present user with air quality data from 3 closest stations.

## APIs
### Geocoding APIs
- [OpenStreetMap](https://www.openstreetmap.org/)
- [Google Maps](https://developers.google.com/maps)

### Weather APIs
- [OpenWeather](https://openweathermap.org/)
- [WeatherAPI](https://www.weatherapi.com/)
- [WeatherStack](https://weatherstack.com/)

### Air quality APIs
- [Portal Jakości Powietrza GIOŚ](https://powietrze.gios.gov.pl/pjp/home)
- [Airly](https://airly.org/)

## Notable libraries
- [System.Device.Location](https://docs.microsoft.com/en-us/dotnet/api/system.device.location)
- [Moq](https://www.nuget.org/packages/Moq)

