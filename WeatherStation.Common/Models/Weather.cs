﻿namespace WeatherStation.Common.Models
{
    /// <summary>
    /// Represents the data about the weather - conditions, temperature, chance of rain.
    /// </summary>
    public class Weather
    {
        /// <summary>
        /// Represents the description about the weather conditions
        /// </summary>
        public string Conditions { get; set; }
        /// <summary>
        /// Represents the temperature
        /// </summary>
        public float Temperature { get; set; }
        /// <summary>
        /// Represents the rainfall
        /// </summary>
        public float ChanceOfRain { get; set; }
        /// <summary>
        /// Represents the location for which weather data is fetched
        /// </summary>
        public ExtendedLocation Location { get; set; }
    }
}
