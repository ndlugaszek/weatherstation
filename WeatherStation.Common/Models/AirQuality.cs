﻿namespace WeatherStation.Common.Models
{
    /// <summary>
    /// Represents the information about the air pollution - PM2.5 and PM10
    /// </summary>
    public class AirQuality
    {
        /// <summary>
        /// Particulate metter 2.5 μm
        /// </summary>
        public float Pm25 { get; set; }

        /// <summary>
        /// Particulate metter 10 μm
        /// </summary>
        public float Pm10 { get; set; }
    }
}
