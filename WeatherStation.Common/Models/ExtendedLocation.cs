﻿namespace WeatherStation.Common.Models
{
    /// <inheritdoc cref="Location"/>
    public class ExtendedLocation : Location
    {
        /// <summary>
        /// Represents the information about the address at the given coordinates
        /// </summary>
        public string Address { get; private set; }

        /// <summary>
        /// The constructor, which creates the new instance of the class and represents the data about the whole location.
        /// </summary>
        /// <param name="address">String with address</param>
        /// <param name="location">Location with latitude and longitude</param>
        public ExtendedLocation(string address, Location location) : base(location.Latitude, location.Longitude)
        {
            this.Address = address;
        }
    }
}