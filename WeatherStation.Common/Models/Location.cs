﻿using System;
using System.Globalization;

namespace WeatherStation.Common.Models
{
    /// <summary>
    /// Represents the data about the location. Provides static method TryParse.
    /// </summary>
    public class Location
    {
        /// <summary>
        /// Represents the instance CultureInfo to set the polish number formatting
        /// </summary>
        private static readonly CultureInfo CULTURE_INFO = new("pl-PL");
        /// <summary>
        /// Represents the array of separators, which can be used in input stream
        /// </summary>
        private static readonly char[] SEPARATORS = { '\\', '|', ';', ':', '/', ' ' };
        /// <summary>
        /// Represents the Float instance with latitude information
        /// </summary>
        public float Latitude { get; private set; }
        /// <summary>
        /// Represents the Float instance with longitude information
        /// </summary>
        public float Longitude { get; private set; }

        public Location() { }

        public Location(float latitude, float longitude)
        {
            if (latitude > 90 || latitude < -90)
            {
                throw new ArgumentException("Latitude is out of range.", nameof(latitude));
            }

            if (longitude > 180 || longitude < -180)
            {
                throw new ArgumentException("Longitude is out of range.", nameof(longitude));
            }

            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        /// <summary>
        /// Takes the location given with user input (latitude, longitude) 
        /// and check if the given input is correct to create the location object
        /// </summary>
        /// <param name="s">String with latitude and longitude</param>
        /// <param name="result">Creation of the <c>Location</c> object, which will be returned if parse was successfully</param>
        /// <returns><c>true</c> if s was converted successfully; otherwise, false.</returns>
        public static bool TryParse(string s, out Location result)
        {
            result = new();

            if (string.IsNullOrWhiteSpace(s) || s.IndexOfAny(SEPARATORS) < 0)
            {
                return false;
            }

            CULTURE_INFO.NumberFormat.NumberDecimalSeparator = ".";
            var coordinates = s.Split(SEPARATORS);

            if (coordinates.Length != 2)
            {
                return false;
            }

            if (!float.TryParse(coordinates[0], NumberStyles.Any, CULTURE_INFO, out float latitude))
            {
                return false;
            }

            if (!float.TryParse(coordinates[1], NumberStyles.Any, CULTURE_INFO, out float longitude))
            {
                return false;
            }

            if (latitude > 90 || latitude < -90)
            {
                return false;
            }

            if (longitude > 180 || longitude < -180)
            {
                return false;
            }

            result.Latitude = latitude;
            result.Longitude = longitude;
            return true;
        }
    }
}
