﻿using System;
using System.Collections.Generic;
using WeatherStation.Common.Models;

namespace WeatherStation.Common.Interfaces
{
    /// <summary>
    /// Represents the data about the weather. Provides methods GetCurrentWeather, GetForecastWeather.
    /// </summary>
    public interface IWeather
    {
        /// <summary>
        /// Takes the the user input and returns current weather at the given location
        /// </summary>
        /// <param name="location">Location instance that represents user location with coordinates</param>
        /// <returns>Weather instance with description, temperature and chance of rain at the given location</returns>
        Weather GetCurrentWeather(Location location);

        /// <summary>
        /// Takes the the user input and returns weather forecast for the next three days at the given location
        /// </summary>
        /// <param name="location">Location instance that represents user location with coordinates</param>
        /// <returns>
        /// Dictionary, which contains DateTime keys with date and Weather values with description, 
        /// temperature and chance of rain at the given location
        /// </returns>
        IDictionary<DateTime, Weather> GetWeatherForecast(Location location);
    }
}
