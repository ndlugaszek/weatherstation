﻿using WeatherStation.Common.Models;

namespace WeatherStation.Common.Interfaces
{
    /// <summary>
    /// Represents the data about the air pollution. Provides methods GetAirQuality.
    /// </summary>
    public interface IAirQuality
    {
        /// <summary>
        /// Returns the guality of the air at the given location
        /// </summary>
        /// <param name="location">Location instance that represents user location with coordinates</param>
        /// <returns>An instance of Weather that represents current weather at the given location</returns>
        AirQuality GetAirQuality(Location location);
    }
}
