﻿using System.Collections.Generic;
using WeatherStation.Common.Models;

namespace WeatherStation.Common.Interfaces
{
    /// <summary>
    /// Represents the data about the location. Provides methods Geocode.
    /// </summary>
    public interface IGeocoding
    {
        /// <summary>
        /// Returns the coordinates of the given location
        /// </summary>
        /// <param name="location">String instance that represents user input stream with address</param>
        /// <returns>An instance of IEnumerable that represents list of the locations</returns>
        IEnumerable<Location> Geocode(string location);
    }
}
