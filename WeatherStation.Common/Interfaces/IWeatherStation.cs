﻿namespace WeatherStation.Common.Interfaces

{
    /// <summary>
    /// Represents the data with the information about current weather and forecast weather for the next three days at the given location
    /// </summary>
    public interface IWeatherStation
    {
        /// <summary>
        /// Shows the current weather and the weather forecast for the next three days at the given location
        /// </summary>
        void GetWeather();
    }
}
