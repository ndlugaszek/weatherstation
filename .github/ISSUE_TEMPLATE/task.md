---
name: Task
about: Define the task that will bring us closer to the milestone.
title: "[TASK] Short description"
labels: enhancement
assignees: ndlugaszek

---

**Description**
*Describe in few words what will be the outcome of the task.*

**Acceptance criteria**
*Using bullet points, explain what should be done for the task to be fully implemented, for example:*
- *What interfaces or services should be created.*
- *What and how should be tested.*
- *Other steps.*

**Considerations and notes**
*If you have any additional considerations or notes for the task, specify them here*
