﻿using System;
using System.Collections.Generic;
using System.Linq;
using WeatherStation.Common.Interfaces;
using WeatherStation.Common.Models;

namespace WeatherStation
{
    /// <inheritdoc cref="IWeatherStation"/>
    public class WeatherStationApp : IWeatherStation
    {
        /// <summary>
        /// Represents the symbol of the celsius degree
        /// </summary>
        private const string CELSIUS_DEGREE = "\u00B0C";
        /// <summary>
        /// The numbers of the forecast days
        /// </summary>
        private const int DAY_NUMBER_OF_FORECAST = 3;
        /// <summary>
        /// Declaration of the IWeather service
        /// </summary>
        private readonly IWeather OPEN_WEATHER_SERVICE;

        public WeatherStationApp(IWeather openWeatherService)
        {
            this.OPEN_WEATHER_SERVICE = openWeatherService;
        }

        public void GetWeather()
        {
            Weather current;
            IDictionary<DateTime, Weather> forecast;
            ConsoleKey shouldContinue;

            do
            {
                var location = GetLocation();
                current = OPEN_WEATHER_SERVICE.GetCurrentWeather(location);
                forecast = OPEN_WEATHER_SERVICE.GetWeatherForecast(location);

                string weatherString = WeatherToString(current, forecast);

                Console.WriteLine(weatherString);
                Console.WriteLine("Czy chcesz sprawdzić pogodę dla kolejnej lokalizacji? ENTER dla kolejnego sprawdzenia, dowolny inny klawisz dla wyjścia");

                shouldContinue = Console.ReadKey().Key;
            } while (shouldContinue.Equals(ConsoleKey.Enter));

            Console.WriteLine("\nDziękuję za skorzystanie z serwisu pogodowego :)");
            Console.ReadKey();
        }

        /// <summary>
        /// Takes the console input and tries converts to the Location object
        /// </summary>
        /// <returns>Location object with coordinates</returns>
        private static Location GetLocation()
        {
            Location location;

            Console.WriteLine("Podaj swoje koordynaty:\n");
            string input = Console.ReadLine();

            while (!Location.TryParse(input, out location))
            {
                Console.WriteLine("Podałeś błędne koordynaty!");
                Console.WriteLine("Wprowadź raz jeszcze:\n");
                input = Console.ReadLine();
            }
            return location;
        }

        /// <summary>
        /// Prepers the final string object, which will be show in the app
        /// </summary>
        /// <param name="weather">Weather instance that represents the data about current weather at the given location</param>
        /// <param name="forecast">Dictionary instance of the DateTime and Weather that represents the data about weather forecast
        /// for the next three days at the given location</param>
        /// <returns>String object with final format of the weather information</returns>
        private static string WeatherToString(Weather weather, IDictionary<DateTime, Weather> forecast)
        {
            string weatherForecastString = "";
            var location = weather.Location.Address.Equals(string.Empty) ? $"({weather.Location.Latitude}, {weather.Location.Longitude})" : weather.Location.Address;

            foreach (var dayForecast in forecast.Take(DAY_NUMBER_OF_FORECAST))
            {
                weatherForecastString += WeatherToString(dayForecast);
            }

            return $"Lokalizacja: {location}\n{WeatherToString(weather)}\n{weatherForecastString}";
        }

        /// <summary>
        /// Prepers the final string object, which will contains the current weather information
        /// </summary>
        /// <param name="weather">Weather instance that represents the data about current weather at the given location</param>
        /// <returns>String object with final format of the current weather information</returns>
        private static string WeatherToString(Weather weather)
        {
            return string.Format("\nPogoda w dniu {0} kształtuje się następująco: \n{1}, temperatura wynosi {2}{3} i opady na poziomie {4} mm/h",
                                DateTime.Now.ToShortDateString(), weather.Conditions, weather.Temperature, CELSIUS_DEGREE, weather.ChanceOfRain);
        }

        /// <summary>
        /// Prepers the final string object, which will contains the weather forecast information
        /// </summary>
        /// <param name="forecast">KeyValuePair instance of the DateTime and Weather that represents the data about weather forecast
        /// for the next three days at the given location</param>
        /// <returns>String object with final format of the weather forecast information</returns>
        private static string WeatherToString(KeyValuePair<DateTime, Weather> forecast)
        {
            return string.Format("\nPrognoza na dzień {0} zapowiada się: \n{1}, przewidywana temperatura {2}{3} i opady będą na poziomie {4} mm/h\n",
                                forecast.Key.ToShortDateString(), forecast.Value.Conditions,
                                forecast.Value.Temperature, CELSIUS_DEGREE, forecast.Value.ChanceOfRain);
        }
    }
}
