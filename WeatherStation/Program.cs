﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WeatherStation.Common.Interfaces;
using WeatherStation.OpenWeatherService.Clients;
using WeatherStation.OpenWeatherService.Interfaces;

namespace WeatherStation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IHost host = CreateHostBuilder(args).Build();

            host.Services.GetService<IWeatherStation>().GetWeather();
        }
        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args).ConfigureServices((_, services) =>
                    services.AddSingleton<IWeatherStation, WeatherStationApp>()
                            .AddSingleton<IWeather, OpenWeatherService.Services.OpenWeatherService>()
                            .AddSingleton<IOpenWeatherClient, OpenWeatherClient>());
        }
    }
}
