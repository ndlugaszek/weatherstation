﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WeatherStation.Common.Models;
using WeatherStation.OpenWeatherService.Interfaces;

namespace WeatherStation.Tests
{
    [TestClass]
    public class OpenWeatherServiceTests
    {
        private Mock<IOpenWeatherClient> client;
        private readonly Location LOCATION = new(53.4289f, 14.553f);

        [TestInitialize]
        public void Initialize()
        {
            client = new();
            client.Setup(c => c.CallCurrentWeather(It.IsAny<Location>()))
                .Returns(File
                .ReadAllTextAsync(string.Format(@".{0}Mocks{0}current_weather.json", Path.DirectorySeparatorChar),
                Encoding.UTF8));

            client.Setup(c => c.CallWeatherForecast(It.IsAny<Location>()))
                .Returns(File
                .ReadAllTextAsync(string.Format(@".{0}Mocks{0}weather_forecast.json", Path.DirectorySeparatorChar),
                Encoding.UTF8));
        }

        [TestMethod]
        public void Current_weather_at_the_given_location()
        {
            var sut = new OpenWeatherService.Services.OpenWeatherService(client.Object);

            var weather = sut.GetCurrentWeather(LOCATION);

            Assert.IsInstanceOfType(weather, typeof(Weather));
        }

        [TestMethod]
        public void Weather_forecast_at_the_given_location()
        {
            var sut = new OpenWeatherService.Services.OpenWeatherService(client.Object);

            var forecast = sut.GetWeatherForecast(LOCATION);

            Assert.IsInstanceOfType(forecast, typeof(Dictionary<DateTime, Weather>));
        }

        [TestMethod]
        public void Temperature_at_the_given_location()
        {
            var sut = new OpenWeatherService.Services.OpenWeatherService(client.Object);

            var weather = sut.GetCurrentWeather(LOCATION);
            var temperature = weather.Temperature;

            Assert.IsInstanceOfType(temperature, typeof(float));
        }

        [TestMethod]
        public void Conditions_at_the_given_location()
        {
            var sut = new OpenWeatherService.Services.OpenWeatherService(client.Object);

            var weather = sut.GetCurrentWeather(LOCATION);
            var conditions = weather.Conditions;

            Assert.IsInstanceOfType(conditions, typeof(string));
        }

        [TestMethod]
        public void Weather_forecast_has_date()
        {
            var sut = new OpenWeatherService.Services.OpenWeatherService(client.Object);

            var forecast = sut.GetWeatherForecast(LOCATION);
            var date = forecast.FirstOrDefault().Key;

            Assert.IsInstanceOfType(date, typeof(DateTime));
        }

        [TestMethod]
        public void First_weather_forecast_is_for_tomorrow()
        {
            var tomorrow = DateTime.Now.AddDays(1).ToShortDateString();

            var sut = new OpenWeatherService.Services.OpenWeatherService(client.Object);

            var forecast = sut.GetWeatherForecast(LOCATION);

            var date = forecast.FirstOrDefault().Key.ToShortDateString();

            Assert.AreEqual(tomorrow, date);
        }
    }
}
