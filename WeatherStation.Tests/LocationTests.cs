using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeatherStation.Common.Models;

namespace WeatherStation.Tests
{
    [TestClass]
    public class LocationTests
    {
        [DataRow("53.455 14,555")]
        [DataRow("53.455;-14,555")]
        [DataRow("-53.455 -14,555")]
        [DataTestMethod]
        public void String_with_coordinates_parse_to_Location_object(string input)
        {
            bool successParse = Location.TryParse(input, out _);

            Assert.IsTrue(successParse);
        }

        [DataRow("53.45514,555")]
        [DataRow("53.455-14,555")]
        [DataRow("91.455 -181,555")]
        [DataTestMethod]
        public void String_with_coordinates_not_parse_to_Location_object(string input)
        {
            bool notSuccessParse = Location.TryParse(input, out _);

            Assert.IsFalse(notSuccessParse);
        }
    }
}
